#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <limits.h>

typedef struct _Graph {
  int *matrix;
  int *dist;
  int *preds;
  int size;
  // struct _Graph * next;
} Graph;

// initialize the graph for s.s.s.p
Graph * initialize_graph(int size) {
  int i;
  
  Graph *g = malloc(sizeof(Graph));

  if(!g)
    goto graph_fail;

  srand(clock());
  g->size = size;

  g->matrix = malloc(sizeof(int)*size*size);
  if(!g->matrix)
    goto matrix_fail;
  
  g->preds = malloc(sizeof(int)*size);
  if(!g->preds)
    goto preds_fail;

  g->dist = malloc(sizeof(int)*size);
  if(!g->dist)
    goto dist_fail;
  

  for(i=0; i<size; ++i){
    int j;
    for(j=0; j<i; ++j){
      int pos = i*size+j;
      g->matrix[pos] = rand()%20+1;
      g->matrix[j*size+i] = g->matrix[pos];
    }
    g->matrix[i*size+i] = 0;
    g->dist[i] = 10000;
    g->preds[i] = -1;
  }
  
  return g;

dist_fail:
  free(g->preds);
preds_fail:
  free(g->matrix);
matrix_fail:
  free(g);
graph_fail:
  return NULL;
}

//
int bellmanford(Graph * g, int source) {
  int i;
  
  int* dist = g->dist;
  int* preds = g->preds;
  int* matrix = g->matrix;

  dist[source] = 0;

  for(i=0; i<g->size; ++i){
    int j;
    for(j=0; j<g->size; ++j) {
      int k;
      for(k=0; k<g->size; ++k){
        int pos = j*(g->size)+k;
        if(dist[k] > dist[j] + matrix[pos]){
          if(i!=g->size-1){
            dist[k] = dist[j] + matrix[pos];
            preds[k] = j;
          }
          else{
            return 0; // negative cycle
          }
        }
      } // for k
    } // for j
  } // for i

  return 1;
}

//
void print_graph(Graph * g) {
  // matrix
  int i,j;
  for(i=0; i<g->size; ++i){
    for(j=0; j<g->size; ++j){
      printf("%-6d", g->matrix[i*(g->size)+j]);
    }
    printf("\n");
  }

  //dist
  printf("\n Dist Table: \n");
  for(i=0; i<g->size; ++i)
    printf("%d, ", g->dist[i]);
  
  //preds
  printf("\n Preds Table: \n");
  for(i=0; i<g->size; ++i)
    printf("%d, ", g->preds[i]);

  printf("\n");
}

int main(int argc, char * argv[]){
  int res;
  Graph* g = initialize_graph(4);

  print_graph(g);
  res = bellmanford(g,0);
  print_graph(g);
  if(!res)
    printf("negative cycle!");

  return 0;
}
